class Solution {
    public ListNode insertionSortList(ListNode head) {
         if(head == null || head.next == null)
        return head;
    
    ListNode leftPtr = head, rightPtr = head.next;
    
    while(rightPtr != null) {
        if(rightPtr.val < leftPtr.val) {
            ListNode temp = new ListNode(0,head);
            while(temp.next.val < rightPtr.val) {
                temp = temp.next;
            }
            
            leftPtr.next = rightPtr.next;
            
            if(temp.next == head) {
                rightPtr.next = head;
                head = rightPtr;
            } else {
                rightPtr.next = temp.next;
                temp.next = rightPtr;
            }
            
            rightPtr = leftPtr.next;
            
        } else {
            rightPtr = rightPtr.next;
            leftPtr = leftPtr.next;
        }
    }
    return head;
} 
}
        
    
