class Solution {
    public int minimumBuckets(String street) {
        int ans = 0;
        char[] streetArr = street.toCharArray();
        for(int i = 0; i < streetArr.length; i++) {
            if(streetArr[i] == 'H') {
                if(i-1 >= 0 && streetArr[i-1] == 'B') continue;
                if(i+1 < streetArr.length && streetArr[i+1] == '.') { 
                    streetArr[i+1] = 'B';
                    ans++;
                }
                else if(i-1 >= 0 && streetArr[i-1] == '.') {
                    streetArr[i-1] = 'B';
                    ans++;
                }
                else return -1;
            }
        }
        return ans;
    }
}