class Solution {
    public ListNode partition(ListNode head, int x) {
         ListNode less = new ListNode(0), headL = less;
        ListNode greater = new ListNode(0), headG = greater;
        while (head != null) {
            if (head.val < x) {
                headL.next = head;
                head = head.next;
                headL = headL.next;
            } else {
                headG.next = head;
                head = head.next;
                headG = headG.next;
            }
        }
        headL.next = greater.next;
        headG.next = null;
        return less.next;
    }
}
    