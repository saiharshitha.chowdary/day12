class Solution {
    public int hIndex(int[] citations) {
         int len = citations.length;
        int[] map = new int[len + 1];
        for (int i = 0; i < len; i++) {
            if (citations[i] >= len) map[len]++;
            else map[citations[i]]++;
        }
        for (int i = len; i >= 1; i--) {
            if (map[i] >= i) return i;
            map[i - 1] += map[i];
        }
        return 0;
    }
}
      