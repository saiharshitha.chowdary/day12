class Solution {
  public int numDupDigitsAtMostN(int N) {
        int[] digits = new int[10];
        int digsCount = getDigits(N, digits);

        boolean[] used = new boolean[10];
        boolean numUniq = true;
        int uniq = initPermCount(digsCount);
        for (int i = digsCount - 1; i >= 0; i--) {
            for (int j = (i == digsCount - 1) ? 1 : 0; j < digits[i]; j++) {
                if (!used[j]) {
                    uniq += permCount(digsCount, digsCount - i);
                }
            }
            if (used[digits[i]]) {
                numUniq = false;
                break;
            }
            used[digits[i]] = true;
        }
        if (numUniq) {
            uniq++;
        }
        return N - uniq;
    }

    private int getDigits(int num, int[] digits) {
        int i = 0;
        while (num > 0) {
            digits[i] = num % 10;
            num /= 10;
            i++;
        }
        return i;
    }

    private int initPermCount(int digsCount) {
        int sum = 0;
        for (int i = 1; i < digsCount; i++) {
            sum += 9 * permCount(i, 1);
        }
        return sum;
    }

    private int permCount(int n, int reserved) {
        int hi = 10 - reserved, lo = hi - (n - reserved);
        int res = 1;
        while (hi > lo) {
            res *= hi--;
        }
        return res;
    }
}